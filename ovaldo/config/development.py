import os

from .base import BaseConfig


class DevelopmentConfig(BaseConfig):

    DEBUG = True

    # Database
    DATABASE_URL = os.getenv('DATABASE_URL', 'sqlite://')

    # Cookies
    USER_HASH_COOKIE_DOMAIN = os.getenv('USER_HASH_COOKIE_DOMAIN', 'localhost')
    USER_HASH_COOKIE_SECURE = False

    # Access control
    ALLOWED_ORIGIN = os.getenv('ALLOWED_ORIGIN', '*')
