import os


try:
    environment = os.environ['ENVIRONMENT']
except KeyError:
    raise OSError('\'ENVIRONMENT\' environment variable has not been set')

if environment == 'dev':
    from dotenv import load_dotenv
    load_dotenv()
    from .development import DevelopmentConfig as Config
elif environment == 'prod':
    from .production import ProductionConfig as Config
else:
    error = 'Invalid environment identifier: \'{environment}\''
    raise ValueError(error.format(environment=environment))


from .base import BaseConfig  # noqa

config: BaseConfig = Config()
