import os


class BaseConfig:

    DEBUG = False

    # Database
    DATABASE_URL: str

    # Logging
    LOGGER_GROUP = 'ovaldo'
    LOG_DIR = os.environ['LOG_DIR']
    LOGGING_CONFIG = {
        'version': 1,
        'formatters': {
            'error': {
                'format': '\n\n[%(asctime)s]  %(message)s\n\n%(traceback)s',
                'datefmt': '%d/%b/%Y %H:%M:%S',
            },
        },
        'handlers': {
            'error_log_console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'error',
            },
            'error_log_file': {
                'level': 'INFO',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': os.path.join(LOG_DIR, 'error.log'),
                'maxBytes': 10 * 1024 * 1024,  # 10 MB
                'backupCount': 5,
                'formatter': 'error',
            },
        },
        'loggers': {
            'ovaldo.errors': {
                'handlers': ['error_log_console', 'error_log_file'],
                'level': 'ERROR',
                'propagate': False,
            },
        },
    }

    # Cookies
    USER_HASH_COOKIE_NAME = 'user_hash'
    USER_HASH_COOKIE_MAX_AGE = os.getenv(
        'USER_HASH_COOKIE_MAX_AGE',
        30 * 24 * 60 * 60,  # 1 month
    )
    USER_HASH_COOKIE_DOMAIN: str
    USER_HASH_COOKIE_PATH = '/'
    USER_HASH_COOKIE_SECURE = True

    # Access control
    ALLOWED_ORIGIN: str
    ALLOWED_METHODS = [
        'GET',
        'POST',
    ]
    ALLOWED_HEADERS = [
        'Allow',
        'Content-Type',
        'Content-Length',
        'Set-Cookie',
        'Vary',
        'X-Timezone-Offset'
    ]
    EXPOSED_HEADERS = [
        'Allow',
        'Content-Type',
        'Content-Length',
    ]
    ALLOW_CREDENTIALS = True

    # Zomato
    ZOMATO_API_KEY = os.getenv('ZOMATO_API_KEY')
