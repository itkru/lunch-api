import os

from .base import BaseConfig


class ProductionConfig(BaseConfig):

    # Database
    DATABASE_URL = os.environ['DATABASE_URL']

    # Cookies
    USER_HASH_COOKIE_DOMAIN = os.environ['USER_HASH_COOKIE_DOMAIN']

    # Access control
    ALLOWED_ORIGIN = os.environ['ALLOWED_ORIGIN']
