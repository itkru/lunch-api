import os
import sys

# Add top-level package to path
dir_path = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(dir_path, '..')))

# Set development environment
os.environ['ENVIRONMENT'] = 'dev'
    
from ovaldo.core.main import create_api

application = create_api(create_tables=False)