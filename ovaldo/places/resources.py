import requests

from ovaldo.config import config
from ovaldo.core.resources import Resource
from .models import Place
from ovaldo.activities.models import Activity

class PlaceCollection(Resource):

    def on_get(self, request, response):
        room_id = request.get_param('room_id') or ''
        search = request.get_param('search') or ''
        # Bratislava -> 'lat': '48.1486', 'lon': '17.1077', 'entity_id' => 111
        lat = request.get_param('lat') or '48.1473438'
        lon = request.get_param('lon') or '17.111928'

        url = 'https://developers.zomato.com/api/v2.1/search'
        headers = {'user-key': config.ZOMATO_API_KEY}
        payload = {'q': search, 'lat': lat, 'lon': lon, 'sort': 'real_distance', 'order': 'asc'}

        restaurants = []

        api_response = requests.get(
            url=url,
            headers=headers,
            params=payload,
        )

        if api_response.status_code < 300:
            places_api = api_response.json()
        else:
            places_api = {'restaurants': []};

        for place in places_api['restaurants']:
            restaurants.append({
                'id': place['restaurant']['id'],
                'external_id': place['restaurant']['R']['res_id'],
                'name': place['restaurant']['name'],
                'location': place['restaurant']['location']['locality_verbose']
            })

        places_db = self.session.query(Activity).with_entities(Place.id, Place.name).join(Activity.place).filter(Activity.room_id == room_id, Place.name == search).group_by(Place.id)
        for place in places_db:
            restaurants.append({
                'id': place.id,
                'name': place.name,
            })

        response.media = {'places': restaurants}
