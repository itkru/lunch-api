import json
from marshmallow import fields

from ovaldo.core.schemas import Schema


class PlaceSchema(Schema):
    id = fields.Integer(allow_none=True)
    external_id = fields.Integer(allow_none=True)
    name = fields.String()
    menu = fields.Method('menu_to_json', dump_only=True)

    def menu_to_json(self, obj):
        if obj.menu:
            return json.loads(obj.menu)
        else:
            return None
