from sqlalchemy import Column, Integer, String, Text

from ovaldo.core.models import Model, TimestampMixin

class Place(TimestampMixin, Model):
    external_id = Column(Integer)
    name = Column(String(50), nullable=False)
    menu = Column(Text)

    __tablename__ = 'place'
