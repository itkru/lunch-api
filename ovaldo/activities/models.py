from datetime import datetime

from sqlalchemy import (
    Column, DateTime, desc, Integer, ForeignKey, Table, Text, String,
    UniqueConstraint)
from sqlalchemy.orm import backref, relationship

from ovaldo.core.models import BaseModel, Model, TimestampMixin


activity_user = Table(
    'activity_user',
    BaseModel.metadata,
    Column('activity_id', Integer, ForeignKey('activity.id'), nullable=False),
    Column('user_id', Integer, ForeignKey('user.id'), nullable=False),
    UniqueConstraint('activity_id', 'user_id'),
)


class Activity(TimestampMixin, Model):
    text = Column(Text, nullable=False)
    room_id = Column(Integer, ForeignKey('room.id'), nullable=False)
    place_id = Column(Integer, ForeignKey('place.id'))
    place_name = Column(String(50))
    starts_at = Column(DateTime)
    created_by_id = Column(Integer, ForeignKey('user.id'), name='created_by')

    created_by = relationship('User', foreign_keys=[created_by_id])
    room = relationship('Room')
    place = relationship('Place')
    users = relationship(
        'User',
        secondary=activity_user,
        back_populates='activities',
    )

    __tablename__ = 'activity'
