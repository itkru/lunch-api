from marshmallow import fields, post_load

from ovaldo.core.schemas import Schema
from ovaldo.places.models import Place
from ovaldo.places.schemas import PlaceSchema
from ovaldo.users.schemas import UserSchema
from .models import Activity


class ActivitySchema(Schema):
    place = fields.Nested(PlaceSchema)
    place_name = fields.String()
    text = fields.String(required=True)
    starts_at = fields.DateTime()
    created_by = fields.Nested(UserSchema, dump_only=True)
    users = fields.Nested(UserSchema, many=True, dump_only=True)

    @post_load
    def make_object(self, data):
        return Activity(
            text=data.get('text'),
            starts_at=data.get('starts_at'),
        )
