import json

import requests

from ovaldo.config import config
from ovaldo.core.errors import HTTPNotFound
from ovaldo.core.resources import Resource
from ovaldo.places.models import Place
from ovaldo.rooms.models import Room
from .schemas import ActivitySchema


class ActivityCollection(Resource):

    def on_post(self, request, response, room_id):

        schema = ActivitySchema()
        activity = schema.load(request.media)

        place_data = request.media.get('place')
        place = self.get_place(data=place_data)

        room = self.session.query(Room).get(room_id)
        if not room:
            raise HTTPNotFound(
                message='Room does not exist.',
            )

        activity.place = place
        activity.room = room
        activity.created_by = request.user
        self.session.add(activity)
        self.session.commit()

        response.media = {
            'activity': schema.dump(activity),
        }

    def get_place(self, data):
        if not data:
            return None
        place_id = data.get('id')
        if place_id:
            place = self.session.query(Place).get(place_id)
            return place
        external_id = data.get('external_id')
        if external_id:
            place = self.session.query(Place).filter_by(
                external_id=external_id,
            ).one_or_none()
            if place:
                place.menu = self.get_menu(external_id=external_id)
                self.session.add(place)
                self.session.commit()
                return place
        place = Place(
            name=data.get('name'),
            external_id=external_id,
            menu=self.get_menu(external_id=external_id)
        )
        self.session.add(place)
        self.session.commit()
        return place

    def get_menu(self, external_id):
        if not external_id:
            return None
        url = "https://developers.zomato.com/api/v2.1/dailymenu"
        headers = {'user-key': config.ZOMATO_API_KEY}
        payload = {'res_id': external_id}
        menu_api = requests.get(
            url=url,
            headers=headers,
            params=payload,
        ).json()
        if menu_api['status'] == 'success':
            return json.dumps(menu_api['daily_menus'])
        return None
