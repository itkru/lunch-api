from datetime import datetime, timedelta

from .models import Activity
from ovaldo.users.models import User

def get_today_date(client_timezone_offset):
    todayWithTimezone = datetime.utcnow() - timedelta(minutes=client_timezone_offset)
    return todayWithTimezone.replace(hour=0, minute=0, second=0);

def query_activities_today(session, room_id, client_timezone_offset=0):
    todayWithTimezone = get_today_date(client_timezone_offset)
    return query_activities_for_date(session, room_id, todayWithTimezone.strftime("%Y-%m-%d"))

def query_activities_for_date(session, room_id, date):
    startDay = datetime.strptime(date + " 00:00:00", "%Y-%m-%d %H:%M:%S")
    endDay = datetime.strptime(date + " 23:59:59", "%Y-%m-%d %H:%M:%S")
    return session.query(Activity).filter(Activity.room_id == room_id, Activity.starts_at >= startDay, Activity.starts_at <= endDay).order_by(Activity.starts_at.desc()).all()

def query_activity_today_with_user(session, room_id, user_id, client_timezone_offset=0):
    todayWithTimezone = get_today_date(client_timezone_offset)
    return session.query(Activity).join(Activity.users).filter(Activity.room_id == room_id, Activity.starts_at >= todayWithTimezone, User.id == user_id).order_by(Activity.starts_at.desc()).first()