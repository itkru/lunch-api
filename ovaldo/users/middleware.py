from datetime import datetime

from ovaldo.config import config
from .models import Guest
from .schemas import UserSchema


class GuestMiddleware:
    """
    Middleware for guest user identification using HTTP cookies.
    """

    def process_resource(self, request, response, resource, params):
        """
        Prepare request after routing.
        """
        self.add_guest_user(request=request, resource=resource)

    def add_guest_user(self, request, resource):
        """
        Set guest and user attributes of the request object.
        """
        guest, user = self.get_guest_user(
            request=request,
            session=resource.session,
        )
        if not getattr(request, 'guest', None):
            request.guest = guest
        if not getattr(request, 'user', None):
            request.user = user

    def get_guest_user(self, request, session):
        """
        Get guest and user objects from user hash value.
        """
        user_hash = request.cookies.get(config.USER_HASH_COOKIE_NAME)

        if not user_hash:
            return None, None

        guest = session.query(Guest).filter_by(hash=user_hash).one_or_none()
        if not guest:
            return None, None
        user = guest.user if guest else None

        guest.accessed_at = datetime.utcnow()
        session.add(guest)
        session.commit()

        return guest, user

    def process_response(self, request, response, resource, succeeded):
        """
        Extend response after method execution.
        """
        self.set_cookie(request=request, response=response)
        self.dump_user(request=request, response=response)

    def set_cookie(self, request, response):
        """
        Set hash in cookies if a guest user has been matched or newly created.
        """
        guest = getattr(request, 'guest', None)
        if guest:
            response.set_cookie(
                name=config.USER_HASH_COOKIE_NAME,
                value=guest.hash,
                max_age=config.USER_HASH_COOKIE_MAX_AGE,
                domain=config.USER_HASH_COOKIE_DOMAIN,
                path=config.USER_HASH_COOKIE_PATH,
                secure=config.USER_HASH_COOKIE_SECURE,
            )

    def dump_user(self, request, response):
        """
        Include user data in the response.
        """
        schema = UserSchema()
        if getattr(request, 'user', None):
            response.media['user'] = schema.dump(request.user)
        else:
            response.media['user'] = None
