from datetime import datetime

from sqlalchemy import Column, String, DateTime, Integer, ForeignKey
from sqlalchemy.orm import relationship

from ovaldo.core.models import Model, TimestampMixin
from .utils import generate_user_hash


class User(TimestampMixin, Model):
    name = Column(String(50))

    activities = relationship(
        'Activity',
        secondary='activity_user',
        back_populates='users',
    )

    __tablename__ = 'user'


class Guest(TimestampMixin, Model):
    hash = Column(
        String(36),
        nullable=False,
        unique=True,
        default=generate_user_hash,
    )
    accessed_at = Column(
        DateTime,
        nullable=False,
        default=datetime.utcnow,
    )
    user_id = Column(Integer, ForeignKey('user.id'))

    user = relationship('User', backref='guests')

    __tablename__ = 'guest'
