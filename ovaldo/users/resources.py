from ovaldo.core.errors import HTTPNotFound
from ovaldo.core.resources import Resource
from ovaldo.core.utils import client_timezone_offset_from_request
from ovaldo.activities.models import Activity
from ovaldo.activities.schemas import ActivitySchema
from ovaldo.activities.utils import query_activity_today_with_user
from .constants import UserAction
from .models import Guest
from .schemas import UserActivitySchema, UserSchema
from .utils import requires_user


class UserCollection(Resource):

    def on_post(self, request, response, activity_id=None):
        if activity_id is not None:
            requires_user(request=request)
            self.post_action(
                request=request,
                response=response,
                activity_id=activity_id,
            )
        else:
            self.post_user(
                request=request,
                response=response,
            )

    def post_action(self, request, response, activity_id):
        activity = self.session.query(Activity).get(activity_id)
        if not activity:
            raise HTTPNotFound(message='Activity does not exist.')
        schema = UserActivitySchema()
        action = schema.load(request.media)
        user = request.user

        # Join activity
        if action == UserAction.JOIN:
            joined = query_activity_today_with_user(self.session, activity.room_id, user.id, client_timezone_offset_from_request(request))
            if (not joined is None):
                joined.users.remove(user)
            activity.users.append(user)

        # Leave activity
        if action == UserAction.LEAVE and user in activity.users:
            activity.users.remove(user)

        self.session.add(activity)
        self.session.add(user)
        self.session.commit()
        response.media = {
            'activity': ActivitySchema().dump(activity),
        }

    def post_user(self, request, response):
        schema = UserSchema()
        if request.user:
            schema.context = {'user': request.user}
            schema.partial = True
        user = schema.load(request.media)
        self.session.add(user)
        if user != request.user:
            guest = Guest(user=user)
            self.session.add(guest)
        else:
            guest = request.guest
        self.session.commit()
        request.user = user
        request.guest = guest
        response.media = {
            'user': schema.dump(user),
        }
