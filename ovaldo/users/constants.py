from enum import Enum


class UserAction(Enum):
    JOIN = 'join'
    LEAVE = 'leave'
    NEW = 'new'


ACTIVITY_ACTIONS = [
    UserAction.JOIN.value,
    UserAction.LEAVE.value,
]

USER_ACTIONS = [
    UserAction.NEW.value,
]
