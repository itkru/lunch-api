from uuid import uuid4

from ovaldo.core.errors import HTTPBadRequest


def requires_user(request, response=None, resource=None, params=None):
    """
    Resource hook for asserting that a user has been created.
    """

    if getattr(request, 'user', None) is None:
        raise HTTPBadRequest(message='Name of user must be submitted first.')


def generate_user_hash():
    """
    Generate hash for a new guest user.
    """
    return str(uuid4())