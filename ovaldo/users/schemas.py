from marshmallow import fields, post_load, validate

from ovaldo.core.schemas import Schema
from .constants import UserAction, ACTIVITY_ACTIONS, USER_ACTIONS
from .models import User


class UserSchema(Schema):
    name = fields.String()
    action = fields.String(
        validate=validate.OneOf(USER_ACTIONS),
        load_only=True,
    )

    @post_load
    def make_object(self, data):
        user = self.context.get('user')
        action = data.get('action')
        if user and action != UserAction.NEW.value:
            user.name = data.get('name', user.name)
            return user
        return User(
            name=data.get('name'),
        )


class UserActivitySchema(Schema):
    action = fields.String(
        validate=validate.OneOf(ACTIVITY_ACTIONS),
        required=True,
    )

    @post_load
    def get_enum(self, data):
        return UserAction(data.get('action'))
