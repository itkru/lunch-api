from sqlalchemy import Column, String

from ovaldo.core.models import TimestampMixin, Model


class Room(TimestampMixin, Model):
    name = Column(String(50), nullable=False, unique=True)

    __tablename__ = 'room'