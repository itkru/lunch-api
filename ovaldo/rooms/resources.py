from ovaldo.core.errors import HTTPNotFound
from ovaldo.core.resources import Resource
from ovaldo.core.utils import client_timezone_offset_from_request
from .models import Room
from .schemas import RoomSchema
from ovaldo.activities.utils import query_activities_today, query_activities_for_date

class RoomResource(Resource):

    def on_get(self, request, response, room_id):
        room = self.session.query(Room).get(room_id)
        if (request.get_param("date")):
            room.activities = query_activities_for_date(self.session, room_id, request.get_param("date"))
        else :
            room.activities = query_activities_today(self.session, room_id, client_timezone_offset_from_request(request))
        if room:
            roomSchema = RoomSchema()
            response.media = {
                'room': roomSchema.dump(room),
            }
        else:
            raise HTTPNotFound(
                message='Room does not exist.',
            )


class RoomCollection(Resource):

    def on_get(self, request, response):
        name = request.get_param(name='name')
        if name:
            rooms = self.session.query(Room).filter_by(name=name)
        else:
            rooms = self.session.query(Room)
        schema = RoomSchema(many=True)
        response.media = {
            'rooms': schema.dump(rooms),
        }

    def on_post(self, request, response):
        schema = RoomSchema()
        name = request.media.get('name')
        room = self.session.query(Room).filter_by(name=name).one_or_none()
        if not room:
            room = schema.load(request.media)
            self.session.add(room)
            self.session.commit()
        room.activities = query_activities_today(self.session, room.id, client_timezone_offset_from_request(request))
        response.media = {
            'room': schema.dump(room),
        }
