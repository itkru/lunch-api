from marshmallow import fields, post_load, validate

from ovaldo.activities.schemas import ActivitySchema
from ovaldo.core.schemas import Schema
from .models import Room


class RoomSchema(Schema):
    name = fields.String(
        required=True,
        validate=validate.Length(min=1, max=50),
    )
    is_new = fields.Boolean(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    activities = fields.Nested(
        ActivitySchema,
        many=True,
        dump_only=True,
    )

    @post_load
    def make_object(self, data):
        return Room(
            name=data.get('name'),
        )
