import argparse
import os
import re
import sys

from dotenv import find_dotenv
from werkzeug.serving import run_simple


# Add top-level package to path
dir_path = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(dir_path, '..')))

# Set development environment
os.environ['ENVIRONMENT'] = 'dev'

# Import all necessary subpackages
from ovaldo import *  # noqa


def serve():
    """
    Run development server.
    """

    # Parse arguments
    args = parse_args()

    # Import WSGI application from core module
    from ovaldo.core.main import create_api
    api = create_api(create_tables=args.create_tables)

    # Start server
    run_simple(
        hostname=args.host,
        port=args.port,
        application=api,
        use_reloader=args.reload,
        use_debugger=args.debug,
        extra_files=[find_dotenv()],
    )


def parse_args():
    """
    Parse command line arguments.
    """

    prog, usage = get_usage()

    parser = argparse.ArgumentParser(
        prog=prog,
        usage=usage,
        description='Run development server.',
    )

    parser.add_argument(
        'host:port',
        action=AddressParser,
        default='',
        type=str,
        nargs=argparse.OPTIONAL,
        help='the address the server should listen on',
    )
    parser.add_argument(
        '-c', '--create-tables',
        action='store_true',
        default=False,
        help='create tables from available models',
    )
    parser.add_argument(
        '-n', '--no-reload',
        action='store_false',
        dest='reload',
        help='disable automatic reloading',
    )
    parser.add_argument(
        '-d', '--debug',
        action='store_true',
        help='use Werkzeug\'s debugger',
    )

    args = parser.parse_args()

    return args


def get_usage():
    """
    Get program name and custom usage string.
    """

    # Recreate used command
    prog, cmd = get_command()

    # Format custom usage message
    usage = '{command} [host][:port] [-cnd]'.format(command=cmd)

    return prog, usage


def get_command():
    """
    Recreate command used for invocation.
    """

    # Check for module spec
    spec = globals().get('__spec__', None)

    if spec:
        prog = getattr(spec, 'name', None)
    else:
        prog = None

    # File was invoked as module
    if prog:
        cmd = 'python -m {module}'.format(module=prog)
        return prog, cmd

    # File was invoked as script
    prog = os.path.basename(sys.argv[0])
    cmd = 'python {script}'.format(script=prog)

    return prog, cmd


def parse_address(address):
    """
    Retrieve host and port from address string.
    """

    match = re.match(
        r'^(?P<host>[^\s:]+)?(?::(?P<port>\d+))?$',
        string=address.strip(),
    )

    if not match:
        raise argparse.ArgumentError(
            argument=None,
            message='Invalid address',
        )

    host = match.group('host') or '127.0.0.1'
    port = int(match.group('port') or '5000')

    return host, port


class AddressParser(argparse.Action):
    """
    Argument store action for handling host:port pairs.
    """

    def __call__(self, parser, namespace, values, *args, **kwargs):

        # Parse address value
        address = parse_address(address=values)

        # Split destination into argument names
        dest = self.dest.split(':')

        # Set value of each argument
        for name, value in zip(dest, address):
            setattr(namespace, name, value)


if __name__ == '__main__':
    serve()
