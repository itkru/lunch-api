from collections import OrderedDict

import falcon
from marshmallow import ValidationError

from .utils import log_exception


class HTTPError(falcon.HTTPError):

    def __init__(self, status, message=None, errors=None, headers=None):

        if errors is not None:
            self.errors = errors
        elif message is not None:
            self.errors = [message]
        else:
            self.errors = [status]

        super(HTTPError, self).__init__(
            status=status,
            title=message,
            headers=headers,
        )

    def to_dict(self, obj_type=OrderedDict):
        obj = obj_type()
        obj['errors'] = self.errors
        return obj


class HTTPBadRequest(HTTPError):

    def __init__(self, message=None, errors=None):
        super(HTTPBadRequest, self).__init__(
            status=falcon.HTTP_BAD_REQUEST,
            message=message,
            errors=errors,
        )


class HTTPInternalServerError(HTTPError):

    def __init__(self, exception=None, message=None):
        self.exception = exception
        super(HTTPInternalServerError, self).__init__(
            status=falcon.HTTP_INTERNAL_SERVER_ERROR,
            message=message,
        )


class HTTPNotFound(HTTPError):

    def __init__(self, message=None):
        super(HTTPNotFound, self).__init__(
            status=falcon.HTTP_NOT_FOUND,
            message=message,
        )


def handle_generic_error(exception: Exception, request, response, params):
    """
    Handle generic exceptions.

    Responds with internal server error status.
    """
    log_exception(exception=exception, request=request)
    raise HTTPInternalServerError(
        exception=exception,
        message='Internal server error.',
    )


def handle_http_error(exception: falcon.HTTPError, request, response, params):
    """
    Handle default Falcon HTTP errors.

    Uses custom error serialization by raising a subclass instance.
    """
    raise HTTPError(
        status=exception.status,
        message=exception.title,
        headers=exception.headers,
    )


def handle_method_not_allowed(
        exception: falcon.HTTPMethodNotAllowed, request, response, params):
    """
    Handle Falcon HTTP Method Not Allowed error.
    """

    methods = exception.headers.get('Allow')

    if methods:
        allowed = 'use {methods}'.format(methods=methods)
    else:
        allowed = 'no methods allowed'

    message = 'Method {method} not allowed: {allowed}'.format(
        method=request.method,
        allowed=allowed,
    )

    raise HTTPError(
        status=exception.status,
        message=message,
        headers=exception.headers,
    )


def handle_validation_error(
        exception: ValidationError, request, response, params):
    """
    Handle Marshmallow validation errors.
    """

    errors = []

    for field, messages in exception.normalized_messages().items():
        for message in messages:
            error = 'Validation error in \'{field}\': {message}'.format(
                field=field,
                message=message,
            )
            errors.append(error)

    raise HTTPBadRequest(errors=errors)


def add_error_handlers(api: falcon.API):
    """
    Register custom error handlers.
    """
    api.add_error_handler(
        exception=Exception,
        handler=handle_generic_error,
    )
    api.add_error_handler(
        exception=falcon.HTTPError,
        handler=handle_http_error,
    )
    api.add_error_handler(
        exception=falcon.HTTPMethodNotAllowed,
        handler=handle_method_not_allowed,
    )
    api.add_error_handler(
        exception=ValidationError,
        handler=handle_validation_error,
    )
