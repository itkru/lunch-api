import falcon

from ovaldo.activities.resources import ActivityCollection
from ovaldo.places.resources import PlaceCollection
from ovaldo.rooms.resources import RoomCollection, RoomResource
from ovaldo.users.middleware import GuestMiddleware
from ovaldo.users.resources import UserCollection

from .errors import add_error_handlers
from .middleware import (
    AccessControlMiddleware, ResponseMiddleware, SessionMiddleware)
from .resources import HealthResource
from .utils import create_session, setup_logging


def create_api(create_tables=False):
    """
    Create and initialize a new Falcon API object.

    Returns a callable WSGI application instance.
    """

    session_cls = create_session(create_all=create_tables)

    middleware = [
        SessionMiddleware(session_cls=session_cls),
        ResponseMiddleware(),
        AccessControlMiddleware(),
        GuestMiddleware(),
    ]

    api = falcon.API(
        media_type=falcon.MEDIA_JSON,
        middleware=middleware,
    )

    add_routes(api)
    add_error_handlers(api)
    setup_logging()

    return api


def add_routes(api):
    """
    Route endpoints to their corresponding resources.
    """

    api.add_route(
        '/ovaldo',
        resource=Exception,
    )

    # Rooms
    api.add_route(
        '/rooms',
        resource=RoomCollection(),
    )
    api.add_route(
        '/rooms/{room_id:int}',
        resource=RoomResource(),
    )

    # Activities
    api.add_route(
        '/rooms/{room_id:int}/activities',
        resource=ActivityCollection(),
    )

    # Users
    api.add_route(
        '/users',
        resource=UserCollection(),
    )
    api.add_route(
        '/activities/{activity_id:int}/users',
        resource=UserCollection(),
    )

    # Places
    api.add_route(
        '/places',
        resource=PlaceCollection(),
    )

    # Health
    api.add_route(
        '/status',
        resource=HealthResource(),
    )
