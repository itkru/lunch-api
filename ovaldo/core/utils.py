import logging
import logging.config
from traceback import TracebackException

import falcon
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from ovaldo.config import config
from .models import BaseModel


def create_session(create_all=False):
    """
    Create database engine and session.

    If specified, also create all tables based on declared models.
    """

    engine = create_engine(config.DATABASE_URL)

    if create_all:
        BaseModel.metadata.create_all(bind=engine)

    session_factory = sessionmaker(bind=engine)
    return scoped_session(session_factory=session_factory)


def setup_logging():
    logging.config.dictConfig(config=config.LOGGING_CONFIG)


def log_exception(exception: Exception, request: falcon.Request):
    logger = logging.getLogger('ovaldo.errors')
    message = format_request_log(request=request)
    traceback = TracebackException.from_exception(exc=exception)
    extra = {'traceback': ''.join(traceback.format())}
    logger.error(msg=message, extra=extra)


def format_request_log(request: falcon.Request):
    log_format = '{client} {method} {path}'
    return log_format.format(
        client=request.remote_addr,
        method=request.method,
        path=request.path,
    )

def client_timezone_offset_from_request(request):
    if (request.headers.get('X-TIMEZONE-OFFSET') is None):
        return 0
    return int(request.headers.get('X-TIMEZONE-OFFSET'))