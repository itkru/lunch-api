import marshmallow
from marshmallow import fields


class BaseSchema(marshmallow.Schema):

    class Meta:
        ordered = True


class Schema(BaseSchema):
    id = fields.Integer(dump_only=True)
