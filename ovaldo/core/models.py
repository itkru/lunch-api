from datetime import datetime

from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base


BaseModel = declarative_base(name='BaseModel')


class Model(BaseModel):
    """
    Base class for database models with an id column.
    """
    id = Column(Integer, primary_key=True)

    __abstract__ = True


class TimestampMixin:
    """
    Mixin for automatic timestamps on create and update.
    """
    created_at = Column(
        DateTime,
        nullable=False,
        default=datetime.utcnow,
    )
    updated_at = Column(
        DateTime,
        nullable=False,
        default=datetime.utcnow,
        onupdate=datetime.utcnow,
    )
