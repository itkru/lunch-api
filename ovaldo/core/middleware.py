from collections import OrderedDict

from ovaldo.config import config


class SessionMiddleware:
    """
    Middleware for managing database sessions.
    """

    def __init__(self, session_cls):
        self.session_cls = session_cls

    def process_resource(self, request, response, resource, params):
        resource.session = self.session_cls()

    def process_response(self, request, response, resource, succeeded):
        self.session_cls.remove()


class ResponseMiddleware:
    """
    Middleware for handling response media objects.
    """

    def process_request(self, request, response):
        response.media = OrderedDict()

    def process_response(self, request, response, resource, succeeded):
        response.media = response.media  # call setter to update response data


class AccessControlMiddleware:
    """
    Middleware for setting HTTP headers necessary for CORS.
    """

    def process_response(self, request, response, resource, succeeded):
        response.set_header(
            name='Access-Control-Allow-Origin',
            value=config.ALLOWED_ORIGIN,
        )
        response.set_header(
            name='Access-Control-Allow-Methods',
            value=','.join(config.ALLOWED_METHODS),
        )
        response.set_header(
            name='Access-Control-Allow-Headers',
            value=','.join(config.ALLOWED_HEADERS),
        )
        response.set_header(
            name='Access-Control-Expose-Headers',
            value=','.join(config.EXPOSED_HEADERS),
        )
        if config.ALLOW_CREDENTIALS:
            response.set_header(
                name='Access-Control-Allow-Credentials',
                value='true',
            )
