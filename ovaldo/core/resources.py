import falcon
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session


class Resource:
    """
    Base class for all resources.
    """
    session: Session = None


class HealthResource(Resource):
    """
    Resource for checking availability of the API.

    Returns an empty response with status code HTTP 200.
    """

    def on_get(self, request, response):
        try:
            self.session.execute('SELECT 1')
        except SQLAlchemyError:
            response.status = falcon.HTTP_SERVICE_UNAVAILABLE
        else:
            response.status = falcon.HTTP_OK
