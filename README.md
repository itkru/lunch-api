# Ovaldo API

REST API written in Python using the Falcon framework.


## Usage

Get the latest release of Python 3 and install dependencies.

```
pipenv install
```

Use `ovaldo.serve` to run the development server.

```
python -m ovaldo.serve
```

Check out the wiki for a more comprehensive guide.
